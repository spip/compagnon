# Changelog

## 3.3.0 - 2025-11-25

### Added

- Installable en tant que package Composer

### Changed

- spip/spip#5460 `style_prive_plugin_compagnon` sans compilation de SPIP
- Compatible SPIP 5.0.0-dev

### Fixed

- spip/spip#5460 Utiliser les propriétés logiques dans la CSS de l'espace privé
- HTML5: Retrait des `CDATA` et `text/javascript` dans les balises `<script>`
